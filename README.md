# Introduction

The goal of this project is to learn how applications can communicate events to other applications.

![docs/puml/export/architecture.png](docs/readme/puml_architecture.png)

The project provides 4 microservices (one microservice deployed as 4 different microservices): product, customer, cart, 
and order service. These apps notify other apps using a message broker that is configured with an exchange named **exchange.*.messages**, which is of type **topic** 
([for more information on RabbitMQ exchanges, please see here](https://www.cloudamqp.com/blog/part4-rabbitmq-for-beginners-exchanges-routing-keys-bindings.html)). 
The exchange copies received messages to queues that match a specific bind pattern. Messages are then routed to one or 
more queues based on matching between the message routing key and the bind pattern.

Queues can be defined directly in the message broker (RabbitMQ), or can be created by consumers (e.g., queues defined in 
the Symfony Messenger package). 

This project provides 2 ways to declare the exchanges: **static**, driven by RabbitMQ definitions, and **dynamic**, driven by Symfony/Messenger configuration.

## Local development

Project has defined local environment provided by [docker compose V2](https://docs.docker.com/compose/).

The easiest way to run it is to execute commands:

```shell
docker compose -f docker-compose.yaml -f docker-compose.static.product.yaml -f docker-compose.static.customer.yaml -f docker-compose.static.cart.yaml -f docker-compose.static.order.yaml -f docker-compose.dynamic.product.yaml -f docker-compose.dynamic.customer.yaml -f docker-compose.dynamic.cart.yaml -f docker-compose.dynamic.order.yaml build
docker compose -f docker-compose.yaml -f docker-compose.static.product.yaml -f docker-compose.static.customer.yaml -f docker-compose.static.cart.yaml -f docker-compose.static.order.yaml -f docker-compose.dynamic.product.yaml -f docker-compose.dynamic.customer.yaml -f docker-compose.dynamic.cart.yaml -f docker-compose.dynamic.order.yaml up
```

## Exchange & Queue

The project provides exchanges in two ways: as static RabbitMQ configuration and dynamic application-driven configuration, both of which serve the same purpose.

![docs/resources/messaging/exchange.png](docs/readme/rabbitmq_exchange_static.png)
![docs/resources/messaging/exchange.png](docs/readme/rabbitmq_exchange_dynamic.png)

To consume messages from queue, you can start consumers via below commands:

##### static

```shell
docker exec -it microservices-messaging-static-product-1   bin/console messenger:consume consume-static --queues=queue.static.product
docker exec -it microservices-messaging-static-customer-1  bin/console messenger:consume consume-static --queues=queue.static.customer
docker exec -it microservices-messaging-static-cart-1      bin/console messenger:consume consume-static --queues=queue.static.cart
docker exec -it microservices-messaging-static-order-1     bin/console messenger:consume consume-static --queues=queue.static.order
```

##### dynamic

```shell
docker exec -it microservices-messaging-dynamic-product-1   bin/console messenger:consume dynamic --queues=queue.dynamic.product
docker exec -it microservices-messaging-dynamic-customer-1  bin/console messenger:consume dynamic --queues=queue.dynamic.customer
docker exec -it microservices-messaging-dynamic-cart-1      bin/console messenger:consume dynamic --queues=queue.dynamic.cart
docker exec -it microservices-messaging-dynamic-order-1     bin/console messenger:consume dynamic --queues=queue.dynamic.order
```

###### _Tip: You need to run one consumer in one process._

## Examples

Example possible messages scenario:

##### static

```shell
docker exec -it microservices-messaging-static-customer-1  bin/console app:notify customer.account.created          "[Customer#123]"            "Customer 123 account created."
docker exec -it microservices-messaging-static-customer-1  bin/console app:notify customer.account.activated        "[Customer#123]"            "Customer 123 account activated."
docker exec -it microservices-messaging-static-cart-1      bin/console app:notify cart.items.added                  "[Cart#999,Product#666]"    "Product 666 added to cart 999."
docker exec -it microservices-messaging-static-product-1   bin/console app:notify product.price.changed             "[Product#666]"             "Product price changed from 2.00 USD to 2.99 USD."
docker exec -it microservices-messaging-static-cart-1      bin/console app:notify cart.items.added                  "[Cart#999,Product#777]"    "Product 777 added to cart 999."
docker exec -it microservices-messaging-static-customer-1  bin/console app:notify customer.billing.country.changed  "[Customer#123]"            "Customer 123 billing country changed."
docker exec -it microservices-messaging-static-order-1     bin/console app:notify order.status.created              "[Order#111]"               "Created new order 111."
docker exec -it microservices-messaging-static-order-1     bin/console app:notify order.payment.paid                "[Order#111]"               "Order 111 payment paid."
docker exec -it microservices-messaging-static-order-1     bin/console app:notify order.status.confirmed            "[Order#111]"               "Order 111 confirmed."
docker exec -it microservices-messaging-static-order-1     bin/console app:notify order.status.canceled             "[Order#111]"               "Order 111 canceled."
```

![docs/resources/messaging/exchange.png](docs/readme/examples_static_1.png)

##### dynamic

```shell
docker exec -it microservices-messaging-dynamic-customer-1  bin/console app:notify customer.account.created          "[Customer#123]"            "Customer 123 account created."
docker exec -it microservices-messaging-dynamic-customer-1  bin/console app:notify customer.account.activated        "[Customer#123]"            "Customer 123 account activated."
docker exec -it microservices-messaging-dynamic-cart-1      bin/console app:notify cart.items.added                  "[Cart#999,Product#666]"    "Product 666 added to cart 999."
docker exec -it microservices-messaging-dynamic-product-1   bin/console app:notify product.price.changed             "[Product#666]"             "Product price changed from 2.00 USD to 2.99 USD."
docker exec -it microservices-messaging-dynamic-cart-1      bin/console app:notify cart.items.added                  "[Cart#999,Product#777]"    "Product 777 added to cart 999."
docker exec -it microservices-messaging-dynamic-customer-1  bin/console app:notify customer.billing.country.changed  "[Customer#123]"            "Customer 123 billing country changed."
docker exec -it microservices-messaging-dynamic-order-1     bin/console app:notify order.status.created              "[Order#111]"               "Created new order 111."
docker exec -it microservices-messaging-dynamic-order-1     bin/console app:notify order.payment.paid                "[Order#111]"               "Order 111 payment paid."
docker exec -it microservices-messaging-dynamic-order-1     bin/console app:notify order.status.confirmed            "[Order#111]"               "Order 111 confirmed."
docker exec -it microservices-messaging-dynamic-order-1     bin/console app:notify order.status.canceled             "[Order#111]"               "Order 111 canceled."
```

![docs/resources/messaging/exchange.png](docs/readme/examples_dynamic_1.png)

###### _Tip: you can execute them all in one process._