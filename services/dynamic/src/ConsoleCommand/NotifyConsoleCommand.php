<?php

declare(strict_types=1);

namespace App\ConsoleCommand;

use App\Message\Message;
use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Messenger\Bridge\Amqp\Transport\AmqpStamp;
use Symfony\Component\Messenger\Envelope;
use Symfony\Component\Messenger\MessageBusInterface;

#[AsCommand(name: 'app:notify')]
class NotifyConsoleCommand extends Command
{
    public function __construct(
        private readonly MessageBusInterface $messageBus,
    ) {
        parent::__construct();
    }

    protected function configure(): void
    {
        $this->addArgument('key', InputArgument::REQUIRED);
        $this->addArgument('title', InputArgument::REQUIRED);
        $this->addArgument('body', InputArgument::REQUIRED);
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $msg = new Message(
            $input->getArgument('key'),
            $input->getArgument('title'),
            $input->getArgument('body')
        );
        $envelope = new Envelope($msg, [new AmqpStamp($input->getArgument('key'))]);
        $this->messageBus->dispatch($envelope);

        return Command::SUCCESS;
    }
}
