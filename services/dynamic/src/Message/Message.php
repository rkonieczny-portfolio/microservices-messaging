<?php

declare(strict_types=1);

namespace App\Message;

final class Message
{
    public function __construct(
        public string $routingKey,
        public string $title,
        public string $body,
    ) {
    }

    public function getRoutingKey(): string
    {
        return $this->routingKey;
    }

    public function getTitle(): string
    {
        return $this->title;
    }

    public function getBody(): string
    {
        return $this->body;
    }
}
