<?php

declare(strict_types=1);

namespace App\MessageHandler;

use App\Message\Message;
use Symfony\Component\Messenger\Attribute\AsMessageHandler;

#[AsMessageHandler]
class MessageHandler
{
    private const MSG_MAP = [
        'product' => [
            'product.#' => [
                'The ProductService handles the product message.',
            ],
        ],
        'customer' => [
            'customer.#' => [
                'The CustomerService handles the customer message.',
            ],
            'order.status.created' => [
                'The CustomerService handles the order create message.',
                'Adding note to customer about new order.',
                'Sending email notification about new order.',
                'Sending push-up notification about new order.',
            ],
            'order.status.canceled' => [
                'The CustomerService handles the order cancel message.',
                'Adding note to customer about canceled order.',
                'Send email notification about canceled order.',
                'Send push-up notification about canceled order.',
            ],
        ],
        'cart' => [
            'cart.#' => [
                'The CartService handles the cart message.',
            ],
            'product.price.#' => [
                'The CartService handles the product price message.',
                'Recalculate cart items prices',
                'Recalculate cart amount',
            ],
            'customer.billing.country.changed' => [
                'The CartService handles the customer billing country change message.',
                'Recalculate cart items prices',
                'Recalculate cart shipping prices',
                'Recalculate cart amount',
            ],
        ],
        'order' => [
            'order.#' => [
                'The OrderService handles the order message.',
            ],
        ],
        'default' => [
            'default' => [
                'Doing crazy things in system.',
            ]
        ]
    ];

    public function __construct(
        private readonly string $app,
    ) {
    }

    public function __invoke(Message $message)
    {
        echo sprintf(PHP_EOL.'Message: "%s".', $message->routingKey);
        $mapped = null;

        $routingKey = $message->routingKey;;

        do {
            $variations = [
                $routingKey,
                $routingKey.'.#',
                $routingKey.'.*',
            ];

            foreach ($variations as $variation) {
                if (isset(self::MSG_MAP[$this->app][$variation])) {
                    $mapped = self::MSG_MAP[$this->app][$variation];
                    break 2;
                }
            }

            $exploded = explode('.', $routingKey);
            array_pop($exploded);
            $routingKey = implode('.', $exploded);
        } while (!empty($routingKey));

        if (null === $mapped) {
            $mapped = self::MSG_MAP['default']['default'];
        }

        echo sprintf("\n ~ ".$mapped[0], $message->title, $message->body);
        array_shift($mapped);

        foreach ($mapped as $item) {
            echo sprintf("\n    - ".$item, $message->title, $message->body);
        }

        echo PHP_EOL;
    }
}
